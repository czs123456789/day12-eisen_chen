import { useDispatch } from 'react-redux'
import './TodoItem.css'
import { loadTodos, toggleTodo } from './apis/todo'
import TodoDelete from './components/TodoDelete'
import { useNavigate } from 'react-router-dom'
import { initTodos } from './components/todoSlice'
import TodoEdit from './components/TodoEdit'


const TodoItem = (props) => {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const change = async (e) => {
        const rotateDiv = document.getElementById(props.item.id)
        rotateDiv.style.animation = "rotateOnClick 1s ease-in-out 1"
        setTimeout(() => {
            rotateDiv.style.animation = ""
        }, 1000);
        if (!props.editable) {
            await toggleTodo(props.item.id, !props.item.done)
            loadTodos().then((response => {
                dispatch(initTodos(response.data))
            }))
        } else {
            navigate(`/todo/${props.item.id}`)
        }
    }


    return (
        <div className={props.item.done ? `todoItemDone now` : `todoItem now `} id={`${props.item.id}`}>
            <div style={{ width: "96%", height: "60px", textAlign: "start", overflowY: "auto" }} onClick={change} >
                {props.item.text}
            </div>
            <div className='icon'>
                {
                    !props.editable && (<TodoEdit id={props.item.id} />)
                }
                {
                    !props.editable && (<TodoDelete id={props.item.id} editable={props.editable} />)
                }
            </div>
        </div>
    )
}

export default TodoItem