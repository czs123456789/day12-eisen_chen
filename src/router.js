import * as React from "react";
import { createBrowserRouter } from "react-router-dom";
import TodoList from './TodoList'
import AboutPage from "./pages/AboutPage";
import DoneList from "./components/DoneList";
import Layout from "./components/Layout";
import TodoDetail from "./TodoDetail";
import NotFoundPage from "./pages/NotFoundPage";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: '/',
        element: <TodoList />
      },
      {
        path: "about",
        element: <AboutPage />,
      },
      {
        path: "done",
        element: <DoneList />
      },
      {
        path: "todo/:id",
        element: <TodoDetail/>
      },
      {
        path: '*',
        element: <NotFoundPage/>
      }
    ]
  }
  
]);
