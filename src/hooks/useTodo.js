import { useDispatch } from "react-redux"
import *  as api from "../apis/todo";
import { initTodos } from "../components/todoSlice";

export const useTodo = () => {
    const dispatch = useDispatch();

    const reloadTodos = () => {
        api.loadTodos().then(response => {
            dispatch(initTodos(response.data))
        })
    }
    return { reloadTodos };
}