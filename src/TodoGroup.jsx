import TodoItom from './TodoItem'
import "./TodoGroup.css"

const TodoGroup = (props)=> {
    const list = props.todoList.map((item,index) => {
        return (
            <TodoItom key={index} item={item} editable={props.editable}/>
        )
    })

    return(
        <>
            <div className='title'>
                ToDoList
            </div>
            
            <div className='TodoGroup'>
                {list}
            </div>
        </>
    )
}


export default TodoGroup