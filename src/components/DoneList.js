import { useSelector } from "react-redux";
import TodoGroup from "../TodoGroup";

const DoneList = () => {
    const todos = useSelector((state) =>
            state.todo
    )
    const doneTodos = todos.todoList.filter((todo) => {
        return todo.done === true 
    })

    return(
    <div>
        <TodoGroup todoList={doneTodos} editable={true}/>
    </div>
    )
}

export default DoneList;