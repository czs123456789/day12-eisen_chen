import { useDispatch } from "react-redux"
import { initTodos } from "./todoSlice";
import './TodoDlete.css'
import { deleteTodo, loadTodos } from "../apis/todo";
import {Button} from "antd";
import {DeleteOutlined} from "@ant-design/icons";
import React from "react";

const TodoDelete = (props) => {
    const dispatch = useDispatch();

    const change = async (e) => {
        if(!props.editable){
            e.stopPropagation()  
            await deleteTodo(props.id)
            loadTodos().then((response => {
                dispatch(initTodos(response.data))
            }))
        }
    }
    return <Button shape="circle" icon={<DeleteOutlined />} onClick={change}></Button>
}

export default TodoDelete