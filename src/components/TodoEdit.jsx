import React, { useEffect, useState } from 'react';
import { Button, Modal } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { loadTodos, updateTodo } from '../apis/todo';
import { initTodos } from './todoSlice';
import { EditOutlined } from '@ant-design/icons';
import TextArea from 'antd/es/input/TextArea';

const TodoEdit = (props) => {
  const list = useSelector(state => state.todo.todoList)
  const todo = list.find(todoI => {
    return todoI.id === props.id
  })
  const navigato = useNavigate()
  const dispatch = useDispatch()

  useEffect(() => {
    if (!todo) {
      navigato("/404")
    }
  }, [todo, navigato])

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [content, setContent] = useState("")
  const showModal = (e) => {
    e.stopPropagation()
    setIsModalOpen(true);
    setContent(todo.text)
  };
  const handleOk = async (e) => {
    setIsModalOpen(false);
    if (content.trim() !== "") {
      await updateTodo(props.id, content)
      loadTodos().then((response => {
        dispatch(initTodos(response.data))
      }))
    }
  };
  const handleCancel = (e) => {
    setIsModalOpen(false);
  };
  const editInputValue = (e) => {
    setContent(e.target.value)
  };
  return (
    <>
      <Button shape="circle" icon={<EditOutlined />} onClick={showModal} />
      <Modal title="Eidt" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} width="50vh">
        <TextArea rows={4} value={content} onChange={editInputValue} />
      </Modal>
    </>
  );
}

export default TodoEdit