import { Outlet } from "react-router";
import { NavLink } from "react-router-dom";
import "./DoneList.css"

const Layout = () => {
    return(
        <div className="layout">
            <div className="first">
                <header>
                    <NavLink to="/">HOME</NavLink>
                    <NavLink to="/about">ABOUT</NavLink>
                    <NavLink to="/done">DONE</NavLink>
                </header>
            </div>
            <div>
                <Outlet/>
            </div>
        </div>
    );
};

export default Layout;