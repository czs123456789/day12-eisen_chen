import { createSlice } from "@reduxjs/toolkit";

const todoSlice = createSlice({
    name:"todo",
    initialState: {
        todoList: []
    },
    reducers: {
        initTodos: (state, action) => {
            state.todoList = action.payload;
        },

        updateTodo: (state, action) => {
            state.todoList = [...state.todoList, action.payload]
        },

        deleteTodo: (state, action) => {
            console.log(action.payload);
            state.todoList = state.todoList.filter(item => item.id !== action.payload)
        },
        
        toggleTodo: (state, action) => {
            const id = action.payload
            state.todoList = state.todoList.map(element => {
                if(id === element.id) {
                    element.done = !element.done
                }
                return element
            })
        }
    }
})

export const { updateTodo,deleteTodo,toggleTodo,initTodos } = todoSlice.actions;

export default todoSlice.reducer