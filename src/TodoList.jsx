import TodoGenerator from "./TodoGenerator"
import TodoGroup from "./TodoGroup"
import { useSelector } from "react-redux"
import "./components/DoneList.css"
import { useEffect } from "react";
import { useTodo } from "./hooks/useTodo";

const TodoList = () => {
    const todoList = useSelector(state => state.todo.todoList)
    // console.log(todoList);
    const { reloadTodos } = useTodo()

    useEffect(() => {
        reloadTodos()
    }, [])

    return(
        <div>

            <TodoGroup todoList={todoList}/>
            <TodoGenerator/>
        </div>
    )
}


export default TodoList