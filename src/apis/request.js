import { message } from "antd";
import axios from "axios";

const request = axios.create({
    // baseURL: "https://64c0c5630d8e251fd11288e0.mockapi.io/",
    baseURL: "http://localhost:8080",
})

request.interceptors.response.use(
    (response) => response,
    (error) => {
        console.log(error.response.data);
        const msg = error.response.data;
        if(msg) {
            message.error(msg);
        }
        return Promise.reject(error);
    }
);

export default request