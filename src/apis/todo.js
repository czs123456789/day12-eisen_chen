import request from "./request";

export const loadTodos = () => {
    return request.get("/todos")
}

export const toggleTodo = (id, done) => {
    return request.put(`/todos/${id}`, {
        done: done
    })
}

export const createTodo = (todo) => {
    return request.post(`/todos`, todo)
}

export const deleteTodo = (id) => {
    return request.delete(`/todos/${id}`)
}

export const updateTodo = (id, text) => {
    return request.put(`/todos/${id}`, {
        text: text,
    })
}

export const findTodoById = (id) => {
    return request.get(`/todos/${id}`)
}