import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { findTodoById } from "./apis/todo";

const TodoDetail = () => {
    const params = useParams()
    const navigato = useNavigate()
    const [todo,setTodo] = useState("");
    const { id } = params;
    

    useEffect(() => {
        findTodoById(id).then(res => {
            console.log(res.data);
            setTodo(res.data)    
            if(!res.data) {
                navigato("/404")
            }       
        });
        
    }, [setTodo, navigato])

    return (
        <div>
            <h1>Detail</h1>
            {todo && (
                <div>
                    <p>{todo.text}</p>
                </div>)}
        </div>
    )
}

export default TodoDetail;