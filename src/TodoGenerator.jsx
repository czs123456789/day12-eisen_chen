import { useDispatch } from "react-redux"
import "./TodoGenerator.css"
import { useState } from "react"
import { initTodos, updateTodo } from "./components/todoSlice"
import { createTodo, loadTodos } from "./apis/todo"
import { async } from "q"
const TodoGenerator = () => {
    const [newItem, setNewItem] = useState("")
    const dispatch = useDispatch();
    const [isOpera, setIsOpera] = useState(false)

    const handlePost = async () => {
        if(newItem.trim() !== '') {
            setIsOpera(true)
            await createTodo({
                text: newItem,
                done: false
            })
            loadTodos().then((response => {
                dispatch(initTodos(response.data))
            }))
        }
        setNewItem("")
        setIsOpera(false)
    }

    function changeInputValue(e) {
        setNewItem(e.target.value)
    }

    const handle = (event) => {
        if(event.keyCode === 13) {
            handlePost()
        }
    }

    return (
        <div>
            <input type="text" value={newItem} onChange={changeInputValue}/>
            <button onClick={handlePost} disabled={isOpera}  onKeyUp={handle}>add</button>
        </div>
    )

}

export default TodoGenerator